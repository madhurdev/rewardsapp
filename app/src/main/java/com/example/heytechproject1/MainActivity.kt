package com.example.heytechproject1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.example.heytechproject1.Fragments.login.PhoneFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment()
    }

    private fun replaceFragment() {
        var fragmentPhone = PhoneFragment()
        var manager : FragmentManager = supportFragmentManager
        manager.beginTransaction().replace(R.id.frameContainer , fragmentPhone).commit()
    }
}