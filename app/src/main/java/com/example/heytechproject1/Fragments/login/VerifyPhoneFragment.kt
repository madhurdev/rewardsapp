package com.example.heytechproject1.Fragments.login

import android.R
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.heytechproject1.Fragments.profile.ProfileFragment
import com.example.heytechproject1.databinding.FragmentVerifyPhoneBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import kotlinx.android.synthetic.main.fragment_phone.*
import kotlinx.android.synthetic.main.fragment_verify_phone.*
import java.util.concurrent.TimeUnit


class VerifyPhoneFragment : Fragment() {

    private lateinit var auth : FirebaseAuth
    lateinit var binding : FragmentVerifyPhoneBinding
    lateinit var verificationId: String
    lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =  FragmentVerifyPhoneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var bundle :Bundle? = arguments
        if(bundle!= null)
        {
            var phoneNumber :String? = bundle.getString("phoneNumber")
            if (phoneNumber != null) {
                sendVerificationCode(phoneNumber)
            }
        }

        buttonSignIn.setOnClickListener {
            var code : String = editTextCode.text.toString().trim()

            if (code.isEmpty() || code.length < 10) {
                editTextPhone?.error = "Valid number is required"
                editTextPhone.requestFocus()
                return@setOnClickListener
            }
            progressBar.visibility = View.VISIBLE
            verifyCode(code)
        }
    }

    private fun sendVerificationCode(phoneNumber: String)
    {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNumber,
            60,
            TimeUnit.SECONDS,
            requireActivity(),
            mCallBack
        )

    }
    private val mCallBack: OnVerificationStateChangedCallbacks =
        object : OnVerificationStateChangedCallbacks() {
            override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken) {
                super.onCodeSent(s, forceResendingToken)
                verificationId = s
            }

            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
            var code : String? = phoneAuthCredential.smsCode
                progressBar.visibility = View.VISIBLE
                code?.let { verifyCode(it) }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
                Log.d("asd" , e.message.toString())
            }
        }

    private fun verifyCode(code: String)
    {
        var credential:PhoneAuthCredential = PhoneAuthProvider.getCredential(verificationId, code)
        signInWithPhoneAuthCredential(credential)

    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity(),
                OnCompleteListener<AuthResult?> { task ->
                    if (task.isSuccessful) {

                        var profilePage = ProfileFragment()
                        var manager : FragmentManager? = fragmentManager
                        manager?.beginTransaction()?.replace(com.example.heytechproject1.R.id.frameContainer , profilePage)?.commit()
                    } else {

                        //verification unsuccessful.. display an error message
                        var message = "Somthing is wrong, we will fix it soon..."
                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            message = "Invalid code entered..."
                        }
                       Toast.makeText(requireContext() , message , Toast.LENGTH_LONG).show()
                    }
                })
    }

}