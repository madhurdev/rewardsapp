package com.example.heytechproject1.Fragments.login

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.heytechproject1.R
import com.example.heytechproject1.databinding.FragmentPhoneBinding
import kotlinx.android.synthetic.main.fragment_phone.*

class PhoneFragment : Fragment() {
    lateinit var binding : FragmentPhoneBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       binding = FragmentPhoneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buttonContinue.setOnClickListener(View.OnClickListener {

            var phoneNumber = editTextPhone.text.toString().trim()

            if (phoneNumber.isEmpty() || phoneNumber.length < 10) {
                editTextPhone.error = "Valid number is required"
                return@OnClickListener
            }
            Log.d("asd" , "button clickd")
            var verifyFragment = VerifyPhoneFragment()
            var manager: FragmentManager? = fragmentManager
            var bundle = Bundle()
            bundle.putString("phoneNumber" , phoneNumber)
            manager?.beginTransaction()?.replace(R.id.frameContainer, verifyFragment)
                ?.commit()
        })

    }
}